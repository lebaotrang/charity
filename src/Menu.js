import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  NavLink
} from "react-router-dom";
function Menu() {
  return (
    <header id="header" className="fixed-top d-flex align-items-center  header-transparent ">
      <div className="container d-flex align-items-center">
        <div className="logo d-none mr-auto d-md-block">
          <h1 className="text-light"><a href="/">Nhóm Từ Thiện PandT</a></h1>
          {/* Uncomment below if you prefer to use an image logo */}
          {/* <a href="index.html"><img src="assets/img/logo.png" alt="" className="img-fluid" /></a> */}
        </div>
        <nav className="nav-menu d-lg-block">
          <ul>
            <li><NavLink exact to="/">Trang chủ</NavLink></li>
            <li><NavLink to="/chien-dich" isActive={(match, location) => {
                  return location.pathname.includes("/chien-dich");
                }}>Chiến dịch</NavLink>
            </li>
            <li><NavLink to="/thong-diep">Thông điệp</NavLink></li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Menu;

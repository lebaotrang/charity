// import { logout } from './auth';
import { toast } from "react-toastify"

export default function handlerError(error) {
    if (error.response.status >= 500) {
        toast.error(`${error.response.statusText}: ${error.response.data}`);
    } else if (error.response.status === 404) {
        if (Array.isArray(error.response.data.message)) {
            error.response.data.message.map(error => {
                toast.error(`ERROR [${error.response.data.code}]: ${error}`);
            });
        } else {
            toast.error(`ERROR [${error.response.data.code}]: ${error.response.data.message}`);
        }
        
    } else if (error.response.status === 401) {
        // setTimeout(() => { logout(); }, 2500);
        toast.error(`ERROR [${error.response.data.statusCode ? error.response.data.statusCode : error.response.data.code}]: ${error.response.data.message}`);
        
        if(error.response.data.message == 'Unauthorized') {
            setTimeout(() => {
                window.location.href="/pages/logout"
            }, 1000);
        }
        console.log('Signature has expired.');
    } else if (error.response.status === 403) {
        // logout();
    } else if (error.response.status === 0) {
        toast.error(`UNKNOW ERROR: Request timeout!`);
    } else if (error.response.status === 402) {
        toast.error( `ERROR [402]: Unknow error!`);
    } else if (error.response.status === 400) {
        
        if (Array.isArray(error.response.data.message)) {
            let len = error.response.data.message.length;
            error.response.data.message.map((error, idx) => {
                // const key = Object.keys(error); // console.log(error);
                if(len==1)
                    toast.error(`ERROR: ${error}`);
                else {
                   if(idx<3)
                    toast.error(`ERROR: ${error}`); 
                }
                
            });
        } else {
            if (typeof error.response.data.message === 'object') {
                const key = Object.keys(error.response.data.message);
                toast.error(`ERROR [${error.response.data.code}]: ${key[0].toUpperCase()}: ${error.response.data.message[key[0]]}`);
            } else {
                toast.error(`ERROR [${error.response.data.code}]: ${error.response.data.message}`);
            }
        }
        
    } else {
        toast.error(`Opps... Unknow error!`);
    }
}


import moment from 'moment';

export function formatDate(date) {
    return moment(new Date(date)).format('DD/MM/YYYY')
}
  
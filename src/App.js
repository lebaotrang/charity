import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Menu from './Menu';
import Carousel  from './Carousel';
import About  from './About';
import Pricing from './Pricing';
import Footer  from './Footer';
import ChienDich  from './ChienDich';
import ChienDichChiTiet  from './ChienDichChiTiet';
import ThongDiepChiTiet  from './ThongDiepChiTiet';
import ThongDiep  from './ThongDiep';
import Home  from './Home';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import AOS from 'aos';
// import 'aos/dist/aos.css';
// AOS.init()

class App extends Component {
  constructor(props) {
    super(props)
    this.myRef = React.createRef()  
}
  componentDidMount() {
    console.log(this.myRef)
    // this.myRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  handleScrollToElement = (event) => {
    // alert();
    // const tesNode = ReactDOM.findDOMNode(this.myRef.test)
    // // if (some_logic){
    //   window.scrollTo(0, tesNode.offsetTop);
    // // }
    console.log(this.myRef.current)
    // this.myRef.current.scrollIntoView()
  }
  render() {
    return (
        <Router>
          <ToastContainer />
          <div>
            <Menu />
            <Carousel handleScrollToElement={this.handleScrollToElement}/>
            <main id="main" ref={ (ref) => this.myRef=ref }>
              <Route path="/" exact component={Home} />
              <Route path="/chien-dich" exact component={ChienDich} />
              <Route path="/chien-dich-chi-tiet/:id" exact component={ChienDichChiTiet} />
              <Route path="/thong-diep" exact component={ThongDiep} />
              <Route path="/thong-diep-chi-tiet/:id" exact component={ThongDiepChiTiet} />
            </main>{/* End #main */}
            {/* ======= Footer ======= */}
            <Footer />
            <a href="#" className="back-to-top"><i className="ri-arrow-up-line" /></a>
          </div>
        </Router>
    );
  }
}

export default App;
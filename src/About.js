
function About() {
  return (
    <section id="about" className="about">
      <div className="container">
        <div className="section-title" data-aos="zoom-out" data-aos-anchor=".main">
          <h2>About</h2>
          <p>Chúng tôi là ai</p>
        </div>
        <div className="row content" data-aos="fade-up">
          <div className="col-lg-6 pt-4 pt-lg-0">
            <img src="/assets/img/who-we-are.jpg" alt="" className="img-fluid" />
          </div>
          <div className="col-lg-6 text-justify">
            <h3>Nhóm từ thiện PandT</h3>
            <p>
            Thật tình mà nói thì chúng tôi chỉ là những người bình thường, trân quý những lời dạy của các bậc giác ngộ. Vì đơn giản, chúng tôi muốn sống hạnh phúc và mang hạnh phúc đến với nhiều người hơn bằng những hành động thiết thực. Mục đích chúng tôi lập ra website là muốn
            </p>
            {/* <p className="font-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua.
            </p> */}
            <ul>
              <li><i className="ri-check-double-line" /> Chia sẻ những hoạt động mà chúng tôi đã thực hiện dù nhỏ nhặt nhất</li>
              <li><i className="ri-check-double-line" /> Gửi đi thật nhiều thông điệp nhưng gói gọn trong một ý nghĩa là <strong>chúng ta hoàn toàn có thể sống một đời sống bình an hạnh phúc CHÂN THẬT.</strong></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;

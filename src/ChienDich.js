import React, { Component } from 'react';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import { Link } from "react-router-dom";

class ChienDich extends Component {
  state = {
    campaigns: [],
    isFetchData: false,
    limit: 10, 
    totalItems: 0
  }
  componentDidMount() {
    this.getCampaigns();
  }

  getCampaigns = async () => {
    this.setState({ isFetchData: true })
    let url = `${apiUrl}/charity-campaign/?page=1&limit=${this.state.limit}`;
    await axios.get(url)
      .then(response => {
        this.setState({ isFetchData: false })
        this.setState({ campaigns: response.data.items, totalItems: response.data.meta.totalItems, isFetchData: false })
        // console.log(response.data.items);
      }).catch(error => {
        this.setState({ isFetchData: false })
        if (error.response)
          handlerError(error);
      })
  }

  loadMore = async () => {
    if(this.state.limit < this.state.totalItems) {
      this.setState({ limit: this.state.limit + 10 }, () => {
        this.getCampaigns();
      })
    }
  }

  render() {
    const { campaigns } = this.state;
    return (
      <section id="services" className="services">
        <div className="container">
          <div className="section-title" data-aos="zoom-out" data-aos-anchor=".main">
            <h2>What we do</h2>
            <p>Chiến dịch</p>
          </div>
          {
            campaigns.map((campaign, idx) => {
              return (
                <div className="row my-4 chiendich" key={idx}>
                  <div className="col-12 col-md-4 p-0">
                    <Link to={"/chien-dich-chi-tiet/" + campaign.slug}>
                      <img src={campaign.image == "default image" ? "/assets/img/default-img.jpg" : campaign.image} className="img-fluid img-thumbnail img-cus" alt="" />
                    </Link>
                  </div>
                  <div className="col-12 col-md-8 p-3">
                    <div className="text-center">
                      <h3><Link to={"/chien-dich-chi-tiet/" + campaign.slug}>{campaign.name}</Link></h3>
                    </div>
                    <div className="text-center d-flex justify-content-around">
                      <div>
                        Từ ngày {formatDate(campaign?.startAt)}
                      </div>
                      <div>
                        Đến ngày {formatDate(campaign?.endAt)}
                      </div>
                      <div>
                        Hiện tại: <strong className="text-danger">{campaign?.totalAmount}</strong> vnđ
                      </div>
                    </div>
                    <div className="row mt-2">
                      <div className="col-12 col-md-6 text-info">
                        <div className="text-info force-overflow" style={{maxHeight: '85px', overflow: 'hidden'}} dangerouslySetInnerHTML={{__html: campaign?.description}}></div>
                      </div>
                      <div className="col-12 col-md-6 text-success text-right">
                        {
                          campaign?.messages && campaign?.messages.length > 0 ?
                          campaign?.messages.map((item, idx) => {
                            if(idx<1) {
                              return (
                                item.title
                              )
                            }
                          }) : 'Chưa có thông điệp'
                        }
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }
          {
            this.state.limit < this.state.totalItems &&
            <div className="col-12 mt-5 text-center">
              <a onClick={this.loadMore} className="btn-danger">Load More</a>
            </div>
          }
        </div>
      </section>
    );
  }
}
export default ChienDich;

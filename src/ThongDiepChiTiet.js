import React, { Component } from 'react';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';

class ThongDiepChiTiet extends Component {
  state = {
    message: undefined,
    contributors: [],
    messages: [],
    message: undefined,
    isFetchData: false,
    title: "",
    name: "",
    content: "",
    limit: 10,
    totalItems: 0
  }
  componentDidMount() {
    this.getMessageDetail(this.props.match.params.id);
  }

  getMessageDetail = async (id) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message/one/${id}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ message: response.data, isFetchData: false})
      this.getMessages(response.data.uuid);
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  getMessages = async (id) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message-comment/${id}?page=1&limit=${this.state.limit}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ messages: response.data.items, totalItems: response.data.meta.totalItems, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }
  
  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  sendComment = async (event) => {
    event.preventDefault();
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message-comment/${this.state.message.uuid}`;
    let data = {
      title: this.state.title,
      name: this.state.name,
      content: this.state.content
    }
    await axios.post(url, data)
    .then(response => {
      this.setState({ isFetchData: false, title: "", name: "", content: ""})
      toast.success("Thank for your comment!")
      this.getMessages(this.state.message.uuid)
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }
  loadMore = async () => {
    if(this.state.limit < this.state.totalItems) {
      this.setState({ limit: this.state.limit + 10 }, () => {
        this.getMessages(this.state.message.uuid);
      })
    }
  }
  render() {
    const { message, messages } = this.state;
    return (
      <>
      <section id="services" className="services">
        <div className="container">
          <div className="section-title" data-aos="zoom-out"  data-aos-anchor=".main">
            <h2>Thông Điệp</h2>
            <p>{message?.title}</p>
          </div>
          <div className="row">
            <div className="col-12 col-md-4">
              <img src={message?.image == "default image" ? "/assets/img/default-img.jpg" : message?.image} className="img-fluid img-cus" alt="" />
            </div>
            <div className="col-12 col-md-8">
              <div className="text-center">
                <h3 className="text-danger">{message?.title}</h3>
                <small><i>Created At: {formatDate(message?.createdAt)}</i></small>
              </div>
              <div className="text-center mt-2 auto-scroll" id="style-4">
                <div className="text-info force-overflow" dangerouslySetInnerHTML={{__html: message?.description}}>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* ======= Features Section ======= */}
      <section id="features" className="features pt-0">
        <div className="container">
          <ul className="nav nav-tabs row d-flex">
            <li className="nav-item col-3" data-aos="zoom-in">
              <a className="nav-link active show" data-toggle="tab" href="#tab-1">
                <i className="ri-gps-line" />
                <h4 className="d-none d-lg-block">Comments</h4>
              </a>
            </li>
          </ul>
          <div className="tab-content" data-aos="fade-up">
            {
              messages && messages.length > 0 && messages.map((item, idx) => {
                  return (
                    <div className="tab-pane active show" id="tab-1" key={idx}>
                      <div className="row mt-3">
                        <div className="col-12">
                          <div className="card card-inner">
                            <div className="card-body" style={{'borderBottom': '1px solid #ccc'}}>
                              <div className="row">
                                <div className="col-md-2 text-center">
                                  <img src="https://image.ibb.co/jw55Ex/def_face.jpg" className="img img-rounded avartar-lg" />
                                  <p className="text-info text-center">{item?.name}</p>
                                </div>
                                <div className="col-md-10">
                                  <p><strong className="text-danger">{item?.title} <small className="text-muted">({formatDate(item?.createdAt)})</small></strong></p>
                                  <p>{item?.content}</p>
                                  {/* <p>
                                    <a className="float-right btn btn-outline-primary ml-2">  <i className="fa fa-reply" /> Reply</a>
                                    <a className="float-right btn text-white btn-danger"> <i className="fa fa-heart" /> Like</a>
                                  </p> */}
                                  {item?.reply && <p class="reply"><i class="icofont-reply icofont-rotate-180"></i> <i>{item?.reply}</i></p>}
                                </div>
                              </div>
                            </div>
                          </div> 
                        </div>
                      </div>
                    </div>
                  )
              })
            }
            {
              this.state.limit < this.state.totalItems &&
              <div className="col-12 mt-5 text-center">
                <a onClick={this.loadMore} className="btn-danger">Load More</a>
              </div>
            }
            {
              messages && messages.length == 0 &&
              <div className="tab-pane active show" id="tab-1">
                  <div className="row mt-3">
                    <div className="col-12">
                      <div className="card card-inner">
                        <div className="card-body" style={{'borderBottom': '1px solid #ccc'}}>
                          <div className="row">
                            <div className="col-md-122 text-center">
                              No commnent available. Please, add your first comment
                            </div>
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
              </div>
            }
            <section id="contact" className="contact">
              <div className="row">
                <div className="col-12 mt-lg-0" data-aos="fade-left">
                  <form onSubmit={this.sendComment} className="php-email-form">
                    <div className="form-row">
                      <div className="col-md-6 form-group">
                        <input type="text" name="name" className="form-control" placeholder="Your Name" required 
                        value={this.state.name} onChange={this.handleChange}/>
                      </div>
                      <div className="col-md-6 form-group">
                        <input type="text" className="form-control" name="title" id="subject" placeholder="Title" required 
                        value={this.state.title} onChange={this.handleChange}/>
                      </div>
                    </div>
                    
                    <div className="form-group">
                      <textarea className="form-control" name="content" rows={5} required placeholder="Message" 
                      value={this.state.content} onChange={this.handleChange} />
                    </div>
                    <div className="text-center">
                      <button type="submit" disabled={this.state.isFetchData}>{this.state.isFetchData ? 'Sending...' : 'Send Message'}</button>
                    </div>
                  </form>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>{/* End Features Section */}
      </>
    );
  }
}
export default ThongDiepChiTiet;
  
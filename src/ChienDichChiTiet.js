import React, { Component } from 'react';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';

class ChienDichDetail extends Component {
  state = {
    campaign: undefined,
    contributors: [],
    messages: [],
    message_id: undefined,
    comments: [],
    isFetchData: false,
    title: "",
    name: "",
    content: "",
    limit_cmt: 10,
    totalItems_cmt: 0
  }
  componentDidMount() {
    this.getCampaignDetail(this.props.match.params.id);
  }

  getCampaignDetail = async (id) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-campaign/${id}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ campaign: response.data, isFetchData: false})
      this.getContributor(response.data.uuid);
      this.getMessages(response.data.uuid);
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  getContributor = async (id_campaign) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-contributor/${id_campaign}/?page=1&limit=10`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ contributors: response.data.items, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  getMessages = async (id_campaign) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message/${id_campaign}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ messages: response.data, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  getCommentsByMessage = async (id_mess) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message-comment/${id_mess}?page=1&limit=${this.state.limit_cmt}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ comments: response.data.items, totalItems: response.data.meta.totalItems, message_id: id_mess, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  sendComment = async (event) => {
    event.preventDefault();
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message-comment/${this.state.message_id}`;
    let data = {
      title: this.state.title,
      name: this.state.name,
      content: this.state.content
    }
    await axios.post(url, data)
    .then(response => {
      this.setState({ isFetchData: false, title: "", name: "", content: ""})
      toast.success("Thank for your comment!")
      this.getCommentsByMessage(this.state.message_id)
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }
  loadMore = async () => {
    if(this.state.limit_cmt < this.state.totalItems) {
      this.setState({ limit_cmt: this.state.limit_cmt + 10 }, () => {
        this.getCommentsByMessage(this.state.message_id);
      })
    }
  }
  
  render() {
    const { campaign, contributors, messages, comments } = this.state;
    return (
      <>
      <section id="services" className="services">
        <div className="container">
          <div className="section-title" data-aos="zoom-out"  data-aos-anchor=".main">
            <h2>Chiến dịch</h2>
            <p>{campaign?.name}</p>
          </div>
          <div className="row">
            <div className="col-12 col-md-4">
              <img src={campaign?.image == "default image" ? "/assets/img/default-img.jpg" : campaign?.image} className="img-fluid img-cus" alt="" />
            </div>
            <div className="col-12 col-md-8">
              <div className="text-center">
                <h3 className="text-danger">{campaign?.name}</h3>
              </div>
              <div className="text-center d-flex justify-content-around">
                <div>
                  Từ ngày {formatDate(campaign?.startAt)}
                </div>
                <div>
                  Đến ngày {formatDate(campaign?.endAt)}
                </div>
                <div>
                  Hiện tại: <strong className="text-danger">{campaign?.totalAmount}</strong> vnđ
                </div>
              </div>
              <div className="text-center mt-2 auto-scroll" id="style-4">
                <div className="text-info force-overflow" dangerouslySetInnerHTML={{__html: campaign?.description}}>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* ======= Features Section ======= */}
      <section id="features" className="features pt-0">
        <div className="container">
          <ul className="nav nav-tabs row d-flex">
            <li className="nav-item col-3" data-aos="zoom-in">
              <a className="nav-link active show" data-toggle="tab" href="#tab-1">
                <i className="ri-gps-line" />
                <h4 className="d-none d-lg-block">Kết quả chiến dịch</h4>
              </a>
            </li>
            <li className="nav-item col-3" data-aos="zoom-in" data-aos-delay={100}>
              <a className="nav-link" data-toggle="tab" href="#tab-2">
                <i className="ri-body-scan-line" />
                <h4 className="d-none d-lg-block">Danh sách đóng góp</h4>
              </a>
            </li>
            {
              messages && messages.length > 0 && messages.map((item, idx) => {
                if(idx < 2) {
                  return (
                    // <li key={idx+100} className="nav-item col-3" data-aos="zoom-in" data-aos-delay={200 + idx*100} onClick={() => this.getMessage(item.uuid)}>
                    <li key={idx+100} className="nav-item col-3" data-aos="zoom-in" data-aos-delay={0 + idx*100} onClick={() => this.getCommentsByMessage(item?.uuid)}>
                      <a className="nav-link" data-toggle="tab" href={'#tab-'+(idx+3)}>
                        <i className={idx==0 ?'ri-sun-line' : 'ri-store-line'} />
                        <h4 className="d-none d-lg-block ellipsis">{item.title}</h4>
                      </a>
                    </li>
                  )
                }
              })
            }
          </ul>
          <div className="tab-content" data-aos="fade-up">
            <div className="tab-pane active show" id="tab-1">
              <div className="row">
                <div className="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
                  {
                    campaign?.result ? <div dangerouslySetInnerHTML={{__html: campaign.result}} ></div> : 'Đang cập nhật...'
                  }
                </div>
                <div className="col-lg-6 order-1 order-lg-2 text-center">
                  <img src={campaign?.image == "default image" ? "/assets/img/default-img.jpg" : campaign?.image} alt="" className="img-fluid" />
                </div>
              </div>
            </div>
            <div className="tab-pane" id="tab-2">
              <div className="row">
                <div className="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Họ tên</th>
                        <th scope="col">Ngày đóng góp</th>
                        <th scope="col">Số tiền</th>
                        <th scope="col">Lời nhắn</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        contributors && contributors.length > 0 && contributors.map((item, idx) => {
                          return (
                            <tr key={idx}>
                              <th scope="row">{idx+1}</th>
                              <td>{item.name}</td>
                              <td>{formatDate(item.contributedAt)}</td>
                              <td>{item.amount}</td>
                              <td>{item.message}</td>
                            </tr>
                          )
                        })
                      }
                      {
                        contributors && contributors.length == 0 && 
                          <tr>
                            <td colSpan="5" className="text-center">Chưa có đóng góp nào</td>
                          </tr>
                      }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            {
              messages && messages.length > 0 && messages.map((item, idx) => {
                if(idx < 2) {
                  return (
                    <div className="tab-pane" id={'tab-'+(idx+3)}>
                      <div className="row">
                        <div className="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
                          <h3><Link to={"/chien-dich-chi-tiet/" + item.slug}>{item?.title}</Link></h3>
                          {/* <div className="">{item?.description}</div> */}
                          <div className="" dangerouslySetInnerHTML={{__html: item?.description}}></div>
                          {/* <ul>
                            <li><i className="ri-check-double-line" /> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                          </ul> */}
                        </div>
                        <div className="col-lg-6 order-1 order-lg-2 text-center">
                          <img src={item?.image == "default image" ? "/assets/img/default-img.jpg" : item?.image} alt="" className="img-fluid" />
                        </div>
                      </div>
                      {
                        comments && comments.length > 0 && comments.map((item, idx) => {
                          return (
                            <div className="row mt-3">
                              <div className="col-12">
                                <div className="card card-inner">
                                  <div className="card-body" style={{'borderBottom': '1px solid #ccc'}}>
                                    <div className="row">
                                      <div className="col-md-2 text-center">
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" className="img img-rounded avartar-lg" />
                                        <p className="text-info text-center">{item?.name}</p>
                                      </div>
                                      <div className="col-md-10">
                                        <p><strong className="text-danger">{item?.title} <small className="text-muted">({formatDate(item?.createdAt)})</small></strong></p>
                                        <p>{item?.content}</p>
                                        {item?.reply && <p class="reply"><i class="icofont-reply icofont-rotate-180"></i> <i>{item?.reply}</i></p>}
                                        {/* <p>
                                          <a className="float-right btn btn-outline-primary ml-2">  <i className="fa fa-reply" /> Reply</a>
                                          <a className="float-right btn text-white btn-danger"> <i className="fa fa-heart" /> Like</a>
                                        </p> */}
                                      </div>
                                    </div>
                                  </div>
                                </div> 
                              </div>
                            </div>
                          )
                        })
                      }

                      {
                        this.state.limit_cmt < this.state.totalItems &&
                        <div className="col-12 mt-5 text-center">
                          <a onClick={this.loadMore} className="btn-danger">Load More</a>
                        </div>
                      }
                      
                      <section id="contact" className="contact">
                        <div className="row">
                          <div className="col-12 mt-lg-0" data-aos="fade-left">
                            <form onSubmit={this.sendComment} className="php-email-form">
                              <div className="form-row">
                                <div className="col-md-6 form-group">
                                  <input type="text" name="name" className="form-control" placeholder="Your Name" required 
                                  value={this.state.name} onChange={this.handleChange}/>
                                </div>
                                <div className="col-md-6 form-group">
                                  <input type="text" className="form-control" name="title" id="subject" placeholder="Title" required 
                                  value={this.state.title} onChange={this.handleChange}/>
                                </div>
                              </div>
                              
                              <div className="form-group">
                                <textarea className="form-control" name="content" rows={5} required placeholder="Message" 
                                value={this.state.content} onChange={this.handleChange} />
                              </div>
                              <div className="text-center">
                                <button type="submit" disabled={this.state.isFetchData}>{this.state.isFetchData ? 'Sending...' : 'Send Message'}</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </section>
                    </div>
                  )
                }
              })
            }
            
          </div>
        </div>
      </section>{/* End Features Section */}
      </>
    );
  }
}
export default ChienDichDetail;
  
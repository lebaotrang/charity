import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './Menu';
import Carousel  from './Carousel';
import About  from './About';
import Contact  from './Contact';
import Pricing from './Pricing';
import Footer  from './Footer';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  NavLink
} from "react-router-dom";

class Home extends Component {
  state = {
    campaigns: [],
    isFetchData: false,
    images: [
      { url: "https://m.baotuyenquang.com.vn/media/images/2020/10/img_20201030103937.jpg", description: "Ủng hộ đồng bào miền trung"},
      { url: "https://duhoc.thanhgiang.com.vn/sites/default/files/1_29.png", description: "Ủng hộ bà con vùng lũ"},
      { url: "https://vnn-imgs-f.vgcloud.vn/2019/05/15/16/tu-thien.jpg", description: "Ủng hộ bà con vùng lũ"},
      { url: "https://www.phatsuonline.com/wp-content/uploads/2020/03/tien-giang-tang-ni-cac-tu-vien-tang-qua-tu-thien-nhan-ky-niem-ngay-duc-phat-niet-ban-225333.jpg", description: "Ủng hộ bà con vùng lũ"},
      { url: "https://image.giacngo.vn/w645/UserImages/2020/11/19/10/Tu%20Nghiem.jpg", description: "Ủng hộ bà con vùng lũ"},
      { url: "http://icdn.dantri.com.vn/zoom/1200_630/kLmtCyYGgph1lHhk1u5h/Image/2014/09/v3-d1900.jpg", description: "Ủng hộ bà con vùng lũ"},
    ]
  }
  
  componentDidMount() {
    this.getCampaigns();
  }

  getCampaigns = async () => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-campaign/?page=1&limit=12`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ campaigns: response.data.items, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }
  render() {
    const { campaigns, images } = this.state;
    return (
      <div>
        {/* <Carousel />
        {/* ======= Hero Section ======= */}
        <main id="main">
          {/* ======= About Section ======= */}
          <About />
          {/* ======= Team Section ======= */}
          <section id="team" className="team">
            <div className="container">
              <div className="section-title" data-aos="zoom-out">
                <h2>Team</h2>
                <p>Chiến dịch</p>
              </div>
              <div className="row">
                {
                  campaigns && campaigns.length>0 && campaigns.map((item, idx) => {
                    return (
                      <div className="col-lg-3 col-md-6 d-flex align-items-stretch" key={idx}>
                        <div className="member" data-aos="fade-up">
                          <div className="member-img">
                            <img src={item.image == "default image" ? "/assets/img/default-img.jpg" : item.image} className="img-fluid img-cus" alt="" />
                            <div className="social">
                              {/* <a href><i className="icofont-twitter" /></a>
                              <a href><i className="icofont-facebook" /></a>
                              <a href><i className="icofont-instagram" /></a>
                              <a href><i className="icofont-linkedin" /></a> */}
                            </div>
                          </div>
                          <div className="member-info">
                            <h4><Link to={"/chien-dich-chi-tiet/"+item.slug}>{item.name}</Link></h4>
                            <span>Ngày {formatDate(item.startAt)} đến {formatDate(item.endAt)}</span>
                          </div>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </section>{/* End Team Section */}
          {/* ======= Team Section ======= */}
          {/* ======= Portfolio Section ======= */}
          <section id="portfolio" className="portfolio">
            <div className="container">
              <div className="section-title" data-aos="zoom-out">
                <h2>Portfolio</h2>
                <p>Hình ảnh</p>
              </div>
              {/* <ul id="portfolio-flters" className="d-flex justify-content-end" data-aos="fade-up">
                <li data-filter="*" className="filter-active">All</li>
                <li data-filter=".filter-app">App</li>
                <li data-filter=".filter-card">Card</li>
                <li data-filter=".filter-web">Web</li>
              </ul> */}
              <div className="row portfolio-container" data-aos="fade-up">
                {
                  images.map((item, idx) => {
                    return (
                      <div className="col-lg-4 col-md-6 portfolio-item filter-app" key={idx+2000}>
                        <div className="portfolio-img"><img src={item.url} className="img-fluid" alt="" /></div>
                        <div className="portfolio-info">
                          <h4>{item.description}</h4>
                          <p>{item.description}</p>
                          <a href={item.url} data-gall="portfolioGallery" className="venobox preview-link" title={item.description}><i className="bx bx-plus" /></a>
                          {/* <a href="portfolio-details.html" className="details-link" title="More Details"><i className="bx bx-link" /></a> */}
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </section>{/* End Portfolio Section */}

          {/* ======= Pricing Section ======= */}
          {/* <Pricing /> */}
          {/* End Pricing Section */}
          {/* ======= F.A.Q Section ======= */}
          {/* <section id="faq" className="faq">
            <div className="container">
              <div className="section-title" data-aos="zoom-out">
                <h2>F.A.Q</h2>
                <p>Frequently Asked Questions</p>
              </div>
              <ul className="faq-list">
                <li data-aos="fade-up" data-aos-delay={100}>
                  <a data-toggle="collapse" className href="#faq1">Non consectetur a erat nam at lectus urna duis? <i className="icofont-simple-up" /></a>
                  <div id="faq1" className="collapse show" data-parent=".faq-list">
                    <p>
                      Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                    </p>
                  </div>
                </li>
                <li data-aos="fade-up" data-aos-delay={200}>
                  <a data-toggle="collapse" href="#faq2" className="collapsed">Feugiat scelerisque varius morbi enim nunc faucibus a pellentesque? <i className="icofont-simple-up" /></a>
                  <div id="faq2" className="collapse" data-parent=".faq-list">
                    <p>
                      Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                    </p>
                  </div>
                </li>
                <li data-aos="fade-up" data-aos-delay={300}>
                  <a data-toggle="collapse" href="#faq3" className="collapsed">Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi? <i className="icofont-simple-up" /></a>
                  <div id="faq3" className="collapse" data-parent=".faq-list">
                    <p>
                      Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                    </p>
                  </div>
                </li>
                <li data-aos="fade-up" data-aos-delay={400}>
                  <a data-toggle="collapse" href="#faq4" className="collapsed">Ac odio tempor orci dapibus. Aliquam eleifend mi in nulla? <i className="icofont-simple-up" /></a>
                  <div id="faq4" className="collapse" data-parent=".faq-list">
                    <p>
                      Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                    </p>
                  </div>
                </li>
                <li data-aos="fade-up" data-aos-delay={500}>
                  <a data-toggle="collapse" href="#faq5" className="collapsed">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i className="icofont-simple-up" /></a>
                  <div id="faq5" className="collapse" data-parent=".faq-list">
                    <p>
                      Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in
                    </p>
                  </div>
                </li>
                <li data-aos="fade-up" data-aos-delay={600}>
                  <a data-toggle="collapse" href="#faq6" className="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i className="icofont-simple-up" /></a>
                  <div id="faq6" className="collapse" data-parent=".faq-list">
                    <p>
                      Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </section> */}
          {/* End F.A.Q Section */}
          
          {/* ======= Contact Section ======= */}
          <Contact />
          {/* End Contact Section */}
        </main>{/* End #main */}
        
      </div>
    );
  }
}
export default Home;

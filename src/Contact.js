import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import { toast } from 'react-toastify';

class Contact extends Component {
  state = {
    isFetchData: false,
    fullname: "",
    message: "",
    subject: "",
    email: "",
  }
  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  sendContact = async (event) => {
    event.preventDefault();
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/contact`;
    let data = {
      fullname: this.state.fullname,
      message: this.state.message,
      subject: this.state.subject,
      email: this.state.email,
    }
    await axios.post(url, data)
    .then(response => {
      this.setState({ isFetchData: false, fullname: "", message: "", subject: "", email: ""})
      toast.success("Thank for your contact us!")
      this.getCommentsByMessage(this.state.message_id)
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }
  render() {
    return (
        <section id="contact" className="contact">
          <div className="container">
            <div className="section-title" data-aos="zoom-out">
              <h2>Contact</h2>
              <p>Contact Us</p>
            </div>
            <div className="row">
              {/* <div className="col-lg-4" data-aos="fade-right">
                <div className="info">
                  <div className="address">
                    <i className="icofont-google-map" />
                    <h4>Location:</h4>
                    <p>A108 Adam Street, New York, NY 535022</p>
                  </div>
                  <div className="email">
                    <i className="icofont-envelope" />
                    <h4>Email:</h4>
                    <p>info@example.com</p>
                  </div>
                  <div className="phone">
                    <i className="icofont-phone" />
                    <h4>Call:</h4>
                    <p>+1 5589 55488 55s</p>
                  </div>
                </div>
              </div> */}
              <div className="col-lg-12 mt-5 mt-lg-0" data-aos="fade-left">
                <form className="php-email-form" onSubmit={this.sendContact} >
                  <div className="form-row">
                    <div className="col-md-6 form-group">
                      <input type="text" name="fullname" className="form-control" placeholder="Your Name" required 
                      value={this.state.fullname} onChange={this.handleChange} disabled={this.state.isFetchData}/>
                    </div>
                    <div className="col-md-6 form-group">
                      <input required type="email" className="form-control" name="email" id="email" placeholder="Your Email" 
                      value={this.state.email} onChange={this.handleChange} disabled={this.state.isFetchData}/>
                    </div>
                  </div>
                  <div className="form-group">
                    <input required type="text" className="form-control" name="subject" id="subject" placeholder="Subject" 
                    value={this.state.subject} onChange={this.handleChange} disabled={this.state.isFetchData}/>
                  </div>
                  <div className="form-group">
                    <textarea required className="form-control" name="message" rows={5}
                    value={this.state.message} onChange={this.handleChange} disabled={this.state.isFetchData}/>
                  </div>
                  <div className="text-center"><button type="submit" disabled={this.state.isFetchData}>{this.state.isFetchData ? 'Sending...' : 'Send Message'}</button></div>
                </form>
              </div>
            </div>
          </div>
        </section>
    );
  }
}

export default Contact;
import React, { Component } from 'react';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import { Link } from "react-router-dom";

class ThongDiep extends Component {
  state = {
    messages: [],
    isFetchData: false,
    limit: 10, 
    totalItems: 0
  }
  componentDidMount() {
    this.getMessages();
  } 

  getMessages = async (param=null) => {
    this.setState({ isFetchData: true})
    let url = `${apiUrl}/charity-message/?page=1&limit=${this.state.limit}`;
    await axios.get(url)
    .then(response => {
      this.setState({ isFetchData: false})
      this.setState({ messages: response.data.items, totalItems: response.data.meta.totalItems, isFetchData: false})
    }).catch(error => {
      this.setState({ isFetchData: false})
      if(error.response)
        handlerError(error);
    })
  }

  loadMore = async () => {
    if(this.state.limit < this.state.totalItems) {
      this.setState({ limit: this.state.limit + 10 }, () => {
        this.getMessages();
      })
    }
  }

  render() {
    const { messages, totalItems } = this.state;
    return (
      <section id="services" className="services">
        <div className="container">
          <div className="section-title" data-aos="zoom-out" data-aos-anchor=".main">
            <h2>What we do</h2>
            <p>Thông Điệp ({totalItems})</p>
          </div>
          <div className="row">
            {
              messages && messages.length > 0 && messages.map((item, idx) => {
                return (
                  <div className="row chiendich" key={idx}>
                    <div className="col-12 col-md-4 p-0">
                      <Link to={"/thong-diep-chi-tiet/" + item.slug}>
                        <img src={item.image == "default image" ? "/assets/img/default-img.jpg" : item.image} className="img-fluid img-thumbnail img-cus" alt="" />
                      </Link>
                    </div>
                    <div className="col-12 col-md-8 m-auto">
                      <div className="text-center">
                        <h3 className="mb-0"><Link to={"/thong-diep-chi-tiet/" + item.slug}>{item.title}</Link></h3>
                        <small className="text-muted">Created at: <i>{formatDate(item.createdAt)}</i></small>
                      </div>
                      <div className="text-center d-flex justify-content-around auto-scroll" id="style-4">
                        <div className="force-overflow" dangerouslySetInnerHTML={{__html: item.description}} ></div> 
                      </div>
                    </div>
                  </div>
                )
              })
            }
          </div>
          {
            this.state.limit < this.state.totalItems &&
            <div className="col-12 mt-5 text-center">
              <a onClick={this.loadMore} className="btn-danger">Load More</a>
            </div>
          }
        </div>
      </section>
    );
  }
}
  
export default ThongDiep;
  
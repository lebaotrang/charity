
function Footer() {
  return (
    <footer id="footer">
      <div className="container">
        <h3>PandT</h3>
        <p>Website được phát triển bởi PandT - Chúc mọi người thân tâm an lạc</p>
        <div className="social-links">
          {/* <a href="#" className="twitter"><i className="bx bxl-twitter" /></a> */}
          <a href="https://www.facebook.com/groups/2633603183597892" className="facebook"><i className="bx bxl-facebook" /></a>
          {/* <a href="#" className="instagram"><i className="bx bxl-instagram" /></a>
          <a href="#" className="google-plus"><i className="bx bxl-skype" /></a>
          <a href="#" className="linkedin"><i className="bx bxl-linkedin" /></a> */}
        </div>
        <div className="copyright">
          © Copyright <strong><span>PandT</span></strong>. All Rights Reserved
        </div>
        <div className="credits">
          {/* All the links in the footer should remain intact. */}
          {/* You can delete the links only if you purchased the pro version. */}
          {/* Licensing information: https://bootstrapmade.com/license/ */}
          {/* Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/selecao-bootstrap-template/ */}
          Designed by <a href="#">PandT</a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

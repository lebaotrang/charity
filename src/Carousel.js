
import React, { Component } from 'react';
import { apiUrl } from './help/constant'
import axios from 'axios';
import handlerError from './help/handlerError'
import { formatDate } from './help/help'
import {
  BrowserRouter as Router,
  Route,
  Link,
  useRouteMatch,
  NavLink
} from "react-router-dom";
class Carousel extends Component {
  state = {
    isFetchData: false,
    images: [
      { url: "/assets/img/lam-dieu-thien-luong.jpg", title: 'Làm điều thiện lương', description: "Cùng chung tay làm những việc thiện lành, giúp đỡ người người xung quanh không phân biệt là lớn lao hay nhỏ nhặt"},
      { url: "/assets/img/lan-toa-yeu-thuong.jpg", title: 'Lan toả yêu thương', description: "Tưới tẩm hạt giống yêu thương trong mỗi chúng ta, dù bạn là người theo dõi chúng tôi hay đồng hành cùng chúng tôi"},
      { url: "/assets/img/gui-gam-nhung-con-duong.jpg", title: 'Gửi gắm những con đường', description: "Cùng dìu dắt và lắng nghe nhau trên con đường hướng thiện, tất cả vì sự bình an nội tại và khỏe mạnh thân tâm"},
    ]
  }
  constructor(props) {
    super(props)
}
  render (){
    const {images} = this.state;
    return (
      <section id="hero" className="d-flex flex-column justify-content-end align-items-center">
        <div id="heroCarousel" className=" carousel carousel-fade w-100" data-ride="carousel">
          {/* Slide 1 */}
          {
            images.map((item, idx) => {
              return (
                <div className={idx == 0 ? 'carousel-item active' : 'carousel-item'} key={idx}>
                  <div className="carousel-container">
                    <img src={item.url} className="d-block w-100 position-absolute" alt="..." style={{height: '90%', 'top': '65px', 'objectFit': 'cover'}}/>
                    <img src="/assets/img/mask.png" className="d-block w-100 position-absolute" alt="..." style={{height: '90%', 'top': '65px', 'objectFit': 'cover'}}/>
                    <h2 className="animate__animated animate__fadeInDown">{item?.title}</h2>
                    <p className="animate__animated fanimate__adeInUp">{item.description}</p>
                    {/* <a className="btn-get-started animate__animated animate__fadeInUp scrollto" onClick={(e) => this.props.handleScrollToElement(e)}>Read More</a> */}
                  </div>
                </div>
              )
            })
          }
          
          <a className="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true" />
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span className="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true" />
            <span className="sr-only">Next</span>
          </a>
        </div>
        <svg className="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
          <defs>
            <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </path></defs>
          <g className="wave1">
            <use xlinkHref="#wave-path" x={50} y={3} fill="rgba(255,255,255, .1)">
            </use></g>
          <g className="wave2">
            <use xlinkHref="#wave-path" x={50} y={0} fill="rgba(255,255,255, .2)">
            </use></g>
          <g className="wave3">
            <use xlinkHref="#wave-path" x={50} y={9} fill="#fff">
            </use></g>
        </svg>
      </section>
    );
  }
}

export default Carousel;
